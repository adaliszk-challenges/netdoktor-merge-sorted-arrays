<?php
namespace AdaLiszk\ArrayManipulation\Sorted\Tests;

use PHPUnit\Framework\TestCase as PHPUnitTestCase;

class TestCase extends PHPUnitTestCase
{
    public function setUp()
    {
        parent::setUp();

        if (!defined('__ROOT_NAMESPACE__'))
            define('__ROOT_NAMESPACE__', str_replace('\\Tests', '\\', __NAMESPACE__));
    }
}