<?php
namespace AdaLiszk\ArrayManipulation\Sorted;

class ArrayMerge
{
    public function __invoke(array ...$arrays): iterable
    {
        $maxIteration = floor(count($arrays, COUNT_RECURSIVE));

        for ($iteration = 1; $iteration < $maxIteration; $iteration++)
        {
            $compare = [];
            foreach ($arrays as $idx => $array)
                if (current($array) !== FALSE) $compare[$idx] = current($array);

            // Dirty check: if no more items then break the loop
            if(empty($compare)) break;

            // Sorting them so the first item would be the next item in order
            asort($compare);

            // Get the id of that item
            $idx = key($compare);

            // Send the item to the output
            yield $compare[$idx];

            // Move it's internal cursor to the next item
            next($arrays[$idx]);

            // Cleanup
            unset($compare);
        }
    }
}
